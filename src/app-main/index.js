import '../App.css';
import React, { Suspense } from 'react';
const RemoteApp = React.lazy(() => import("app2/App"));

function AppMain() {
    return (
        <div className="App">
            <header className="App-header">
                {/* <img src={logo} className="App-logo" alt="logo" /> */}
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="about"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
                <Suspense fallback={"loading..."}>
                    <RemoteApp />
                </Suspense>
            </header>


        </div>
    );
}

export default AppMain;
