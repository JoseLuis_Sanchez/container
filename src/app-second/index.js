import '../App.css';
import React, { Suspense } from 'react';
const RemoteApp = React.lazy(() => import("app2/App"));

function AppSecond() {
    return (
        <Suspense fallback={"loading..."}>
            <RemoteApp />
        </Suspense>
    );
}

export default AppSecond;
