import './App.css';
import React, { Suspense } from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AppMain from './app-main';
import AppSecond from './app-second';

// const RemoteApp = React.lazy(() => import("app2/App"));

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={AppMain} />
        <Route exact path="/app-two" component={AppMain} />
        <Route exact path="/about" component={AppSecond} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
